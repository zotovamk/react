import React from 'react';
import ReactDOM from 'react-dom';
import HeaderBlock from './components/HeaderBlock';
import './index.css';

const AppList = () => {
  const items = ['a', 'b'];
  const isAuth = true;

  return (
    <ul>
      { isAuth ? <li>c</li> : null }
      { items.map(item => <li>{ item }</li> )}
    </ul>
  );
}

const AppHeader = () => {
  return <h1 className='header'>Hello world!</h1>;
}

const AppHeaderInline = () => {
  const margin = 20;
  const headerStyle = {
    color: 'red',
    margibLeft: `${margin}px`
  };
  return <h1 style={ headerStyle }>Hello world!</h1>;
}

const AppInput = () => {
  const placeholder = 'Search...';
  return (
    <label htmlFor="search" className='label'>
      <input id='search' placeholder={placeholder} />
    </label>
  );
}

const App = () => {
  return (
    <>
      <HeaderBlock/>
      <AppHeader/>
      <AppHeaderInline/>
      <AppInput/>
      <AppList/>
    </>
  )
};

ReactDOM.render(<App/>, document.getElementById('root'));